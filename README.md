# Pack
A package manager for Linux.

## License
GPL 3.0 _**or later**_

## Usage
Pack is structured in two parts; A part that handles remote packages and a part that handles locally installed packages.
These parts can be accessed by using `pack remote` and `pack local` respectively.

Installing a package is an operation that pulls in a package from a remote repository, therefore under the `remote` subcommand.  
In order to install a package, use `pack remote install <package>` (Note: you can also install multiple packages at once).

Listing packages can be done in both modes, remote will be listing all available packages and local will be listing all installed packages.  
To do this, use `pack remote --list` and `pack local --list` respectively.

When a subcommand is not provided to `pack`, it is assumed to be `remote`. When a subcommand is not provided to `remote`, it is assumed to be
`install`. Therefore, packages can also be installed just by using `pack <package>`.

## Repository Format
A Pack repository consists of a `Manifest.toml` file and one or more package manifest file. The `Manifest.toml` file is as follows:

``` toml
packages = "Directory/"
```

| Field      | Type   | Description                                                                                           | Required |
| ---        | ---    | ---                                                                                                   | ---      |
| `packages` | String | The name of the directory where all package manifest files are stored (will be searched recursively). | Yes      |

The package manifest file format is as follows:

``` toml
binary = "https://my.server/path/to/curl.tar.gz"
buildscript = "https://my.server/path/to/curl.py"

name = "curl"
version = "7.76.1"
dependencies = ["ca-certificates", "krb5", "libssh2", "libssh2.so=1-64", "openssl", "zlib", "libpsl", "libpsl.so=5-64", "libnghttp2", "libidn2", "libidn2.so=0-64", "zstd"]
provides = ["libcurl.so=4"]

friendly_name = "cURL"
license = "MIT"
description = "A URL retrieval utility"
long_description = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mauris risus, porta quis leo sit amet, tincidunt pretium massa.
Quisque vitae ex eu nulla condimentum bibendum. In auctor libero sed odio elementum, vitae suscipit elit ultrices. Praesent molestie fermentum lectus, ac rutrum urna venenatis eu.
Donec commodo nisi sit amet tortor porttitor, at mollis turpis tincidunt. Vivamus diam sapien, volutpat id tellus sit amet, finibus dignissim lectus.
Proin hendrerit pulvinar ante, vitae tempor purus convallis eget. Praesent lobortis vestibulum libero, vel blandit enim pulvinar a. Pellentesque imperdiet hendrerit elit."""
```

| Field              | Type             | Description                                                                                                                                                                                                      | Required |
| ---                | ---              | ---                                                                                                                                                                                                              | ---      |
| `binary`           | URL              | A path to a GZIpped tarball containing all of the package's binary content                                                                                                                                       | No       |
| `buildscript`      | URL              | A path to a Python script building the package                                                                                                                                                                   | No       |
| `name`             | String           | The unique identifier of this package                                                                                                                                                                            | Yes      |
| `version`          | String           | A SemVer-respecting version                                                                                                                                                                                      | Yes      |
| `dependencies`     | Array<String>    | A list of package names that this package depends on. Optionally, each package may have a SemVer range attached.                                                                                                 | No       |
| `provides`         | Array<String>    | A list of package names that this package provides. This could, for example, be used for multiple library implementations. Optionally, specifying `=<version>` will specify the version of the provided package. | No       |
| `friendly_name`    | String           | The user-friendly name of this package                                                                                                                                                                           | No       |
| `license`          | License (String) | The license that this package is licensed under [(more info)](Sources/pack/Types.swift)                                                                                                                          | Yes      |
| `descrption`       | String           | A short, one-line description of the package                                                                                                                                                                     | Yes      |
| `long_description` | String           | A longer, paragraph description of this package                                                                                                                                                                  | No       |

Note: `binary` and `buildscript` are both optional, however at least one of them has to be specified.

## How are things stored
*Note: All of the paths are hardcoded in [Data.swift](Sources/Pack/Data.swift)*

### Packages
Packages are placed in `/usr/share/pack/pkg/`. Each package gets its own directory there, and all
package files will be in the directory, treating it as if it was root. All of the package files are symlinked to their correct location.

### Cache
The cache directory contains a copy of all package manifest files, downloaded from the specified repositories. It can be cleaned by using `pack --clean` (in order to remove or install a package, its manifest must be downloaded).

### Configuration
The configuration file for Pack is located in `/etc/pack/pack.conf`.
