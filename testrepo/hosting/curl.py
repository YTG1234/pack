#!/usr/bin/python3

import pack
from pack import Actions, HashingMethod, name, version, sums, install_target

def setup():
    return Actions(
        pack.download_file(pack.sources[0], save=f"{name}-{version}.tar.gz"),
        pack.verify_file(f"{name}-{version}.tar.gz", f"{sums[0]}", method=HashingMethod.sha512),
        pack.extract_tarball(f"{name}-{version}.tar.gz", "."),
    )

def build():
    arguments: list[str] = [
        "--prefix=/usr",
        "--mandir=/usr/share/man",
        "--disable-ldap",
        "--disable-ldaps",
        "--disable-manual",
        "--enable-ipv6",
        "--enable-versioned-symbols",
        "--enable-threaded-resolver"
        "--with-gssapi",
        "--with-libssh2",
        "--with-random=/dev/urandom",
        "--with-ca-bundle=/etc/ssl/certs/ca-certificates.crt"
    ]

    return Actions(
        pack.cd(f"{name}-{version}"),
        pack.call("./configure", *arguments),
        pack.call("sed", "-i", "-e", "s/ -shared / -Wl,-O1,--as-needed\\0/g", "libtool"),
        pack.make()
    )

def install():
    return Actions(
        pack.cd(f"{name}-{version}"),

        pack.make("install", {
            "DESTDIR": install_target
        }),
        pack.make("install", {
            "DESTDIR": install_target
        }, "-C scripts"),

        pack.copy_files("COPYING", destDir=pack.license_path)
    )

pack.setup_func = setup
pack.build_func = build
pack.install_func = install
