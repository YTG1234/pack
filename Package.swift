// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

#if !os(Linux)
#error("At this time, only Linux builds are supported.")
#endif

let package = Package(
  name: "Pack",
  products: [
    .library(name: "Pack", targets: ["Pack"]),
    .executable(name: "pack", targets: ["PackExec"]),
  ],
  dependencies: [
    // .package(url: /* package url */, from: "1.0.0"),
    .package(url: "https://github.com/apple/swift-argument-parser", from: "0.4.0"),
    .package(url: "https://github.com/dduan/TOMLDecoder", from: "0.2.1"),
    .package(url: "https://github.com/pvieito/PythonKit", .branch("master")),
  ],
  targets: [
    .target(name: "Pack",
            dependencies: [
              .product(name: "TOMLDecoder", package: "TOMLDecoder")
            ],
            path: "Sources/PackApi"
    ),
    .executableTarget(name: "PackExec",
            dependencies: [
              .product(name: "ArgumentParser", package: "swift-argument-parser"),
              .product(name: "TOMLDecoder", package: "TOMLDecoder"),
              .product(name: "PythonKit", package: "PythonKit"),
              "Pack",
            ],
            path: "Sources/Pack"
    ),
    // .testTarget(
    //     name: "packTests",
    //     dependencies: ["pack"]),
  ]
)
