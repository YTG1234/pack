import Pack

/// Patch this function to add more repo providers.
func addProviders() {
    add(provider: GitProvider.self)
}
