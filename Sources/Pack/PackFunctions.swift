import Foundation
import Pack
import TOMLDecoder
import PythonKit

fileprivate let TMP_BS = "\(PathData.TMP)/buildscript.py"

/// Sync some Pack repositories to a specified folder.
///
/// - parameters:
///   - repos: The repositories to sync. Key = repo name, value = repo URL
///   - folder: Where to download the repos
///
/// Each repository is then downloaded to `folder`.
func syncRepos(_ repos: [String:String], to folder: String) throws {
    for (name, url) in repos {
        let task = Process()
        task.executableURL = URL(fileURLWithPath: "/usr/lib/git-core/git-clone")
        task.currentDirectoryURL = URL(fileURLWithPath: folder)
        task.arguments = [url, folder + "/" + name]

        try task.run()
        task.waitUntilExit()
    }
}

/// Install a package from its manifest object.
///
/// - parameters:
///   - manifest: The manifest object to get information from.
///   - folder: The folder where packags are installed.
func installPackage(from manifest: (Package, URL), using strategy: InstallationStrategy) throws {
    switch strategy {
    case .binary: try installBinary(
                    from: manifest, to: PathData.PACKAGES + "/" + manifest.0.metadata.name,
                    withTempTar: PathData.TMP + "/" + manifest.0.metadata.name + "-" + manifest.0.metadata.version + ".tar.gz",
                    andSaveManifestToDir: PathData.MANIFESTS
                  )
    case .buildscript:
        // We want to build somewhere
        var buildDir: String? = nil

        // Use the XDG base directory specifications if possible
        if let cacheHome = ProcessInfo.processInfo.environment["XDG_CACHE_HOME"] {
            buildDir = cacheHome + "/" + ProgramMetadata.NAME + "/build/" + manifest.0.metadata.name
        } else if let home = ProcessInfo.processInfo.environment["HOME"] {
            buildDir = home + "/.cache/" + ProgramMetadata.NAME + "/build/" + manifest.0.metadata.name
        }

        if let buildDir = buildDir {
            try FileManager.default.createDirectory(
              at: URL(fileURLWithPath: buildDir),
              withIntermediateDirectories: true
            )
        }

        try FileManager.default.createDirectory(
          at: URL(fileURLWithPath: PathData.INST_TGTS + "/" + manifest.0.metadata.name + PathData.LICENSES),
          withIntermediateDirectories: true
        )

        try buildPackage(
          from: manifest,
          in: PythonObject(buildDir ?? PathData.BUILDS + "/" + manifest.0.metadata.name),
          withInstallDir: PythonObject(PathData.INST_TGTS + "/" + manifest.0.metadata.name),
          withLicensePath: PythonObject(PathData.INST_TGTS + "/" + manifest.0.metadata.name + PathData.LICENSES),
          to: PythonObject(PathData.BUILT_PKG + "/" + manifest.0.metadata.name + "-" + manifest.0.metadata.version + ".tar.gz"),
          withTempBuildscript: TMP_BS
        )

        /*
         TODO
         try installBinary(
            from: run { () -> Package in
                var m = manifest
                m.binary = PathData.BUILT_PKG + "/" + manifest.metadata.name + "-" + manifest.metadata.version + ".tar.gz"
                return m
            },
            to: PathData.PACKAGES + "/" + manifest.metadata.name,
            withTempTar: PathData.TMP + "/" + manifest.metadata.name + "-" + manifest.metadata.version + ".tar.gz",
            andSaveManifestToDir: PathData.MANIFESTS
         )
        */
    }
}

func buildPackage(from mData: (Package, URL), in buildDir: PythonObject, withInstallDir installDir: PythonObject,
                  withLicensePath licensePath: PythonObject, to tarballTarget: PythonObject,
                  withTempBuildscript tmpBs: String) throws {
    let manifest = mData.0
    // Download the buildscript
    guard let bs = manifest.retrieval.buildscript else { return }

    let buildscript = (bs.hasPrefix("/") || bs.hasPrefix(".")) ?
      FileManager.default.contents(atPath: mData.1.calculateRPath(to: bs, isDirectory: false).absoluteString)! :
      try sendRequest(target: URL(string: manifest.retrieval.buildscript!)!)

    // Create the temporary file
    if FileManager.default.fileExists(atPath: tmpBs) {
        try FileManager.default.removeItem(at: URL(fileURLWithPath: tmpBs))
    }
    FileManager.default.createFile(atPath: tmpBs, contents: buildscript)

    // Set values for the buildscript to later use
    let packInfo = try Python.attemptImport("pack.info")

    packInfo.name = PythonObject(manifest.metadata.name)
    packInfo.version = PythonObject(manifest.metadata.version)
    packInfo.dependencies = PythonObject(manifest.metadata.dependencies)
    packInfo.provides = PythonObject(manifest.metadata.provides)
    packInfo.succeedes = PythonObject(manifest.metadata.succeedes)
    packInfo.conflicts = PythonObject(manifest.metadata.conflicts)

    packInfo.sources = PythonObject(
      manifest.build.sources.map {
          $0.replaceVar([ "name": manifest.metadata.name, "version": manifest.metadata.version ])
      }
    )
    packInfo.sums = PythonObject(manifest.build.sums)

    packInfo.friendly_name = PythonObject(manifest.metadata2.friendlyName)
    packInfo.license_str = PythonObject(String(manifest.metadata2.license))
    packInfo[dynamicMember: "description"] = PythonObject(manifest.metadata2.description)
    packInfo.long_description = PythonObject(manifest.metadata2.longDescription)

    packInfo.install_target = installDir
    packInfo.license_path = licensePath

    packInfo.tarball_target = tarballTarget
    packInfo.build_dir = buildDir

    packInfo.buildscript_json = PythonObject(String(data: try manifestEncoder.encode(manifest), encoding: .utf8))

    // Run the buildscript
    let importUtil = try Python.attemptImport("importlib.util")
    let spec = importUtil.spec_from_file_location("buildscript", TMP_BS)
    let buildscriptModule = importUtil.module_from_spec(spec)

    spec.loader.exec_module(buildscriptModule)

    // Start the build
    try Python.attemptImport("pack.functions")._start_build()
}

/// Should be run as root
func installBinary(from manifest: (Package, URL), to installDir: String, withTempTar tempTar: String, andSaveManifestToDir saveLocation: String) throws {
    guard let binary = manifest.0.retrieval.binary else { return }

    let binaryData = (binary.hasPrefix("/") || binary.hasPrefix(".")) ?
      FileManager.default.contents(atPath: manifest.1.calculateRPath(to: binary, isDirectory: false).absoluteString)! :
      try sendRequest(target: URL(string: binary)!)

    if FileManager.default.fileExists(atPath: tempTar) {
        try FileManager.default.removeItem(at: URL(fileURLWithPath: tempTar))
    }
    FileManager.default.createFile(atPath: tempTar, contents: binaryData)

    try FileManager.default.createDirectory(at: URL(fileURLWithPath: installDir), withIntermediateDirectories: true)

    let task = Process()
    task.executableURL = URL(fileURLWithPath: "/usr/bin/tar")
    task.arguments = [
      "-x",
      "-f", tempTar,
      "-C", installDir,
      "--no-same-owner",
    ]

    ifDebug { task.arguments?.append("-v") }

    try task.run()
    task.waitUntilExit()

    try FileManager.default.removeItem(at: URL(fileURLWithPath: tempTar))

    try linkInsideDirectory(directory: installDir, fromIdx: installDir.count, ignore: [".VERSION"])

    FileManager.default.createFile(atPath: saveLocation + "/" + manifest.0.metadata.name, contents: try manifestEncoder.encode(manifest.0))
}

func linkInsideDirectory(directory: String, fromIdx: Int, ignore: [String] = []) throws {
    for item in try FileManager.default.contentsOfDirectory(atPath: directory) {
        if ignore.contains(item) { continue }

        let i2 = directory + "/" + item

        // Honestly it's really frustrating when there are no Swift equivalents for the Obj-C stuff
        var b: ObjCBool = false
        if FileManager.default.fileExists(atPath: i2, isDirectory: &b) && b.boolValue {
            try linkInsideDirectory(directory: i2, fromIdx: fromIdx)
            continue
        }

        let fromRoot = i2.substring(from: fromIdx)

        if FileManager.default.fileExists(atPath: fromRoot) {
            print("File \(i2.substring(from: fromIdx)) already exists, refusing to symlink from \(i2).")
            continue
        }

        symlink(i2, fromRoot)
    }
}
