public struct ProgramMetadata {
    public static let NAME      = "pack"
    public static let VERSION   = "0.1.0"
}


public struct PathData {
    public static let PACKAGES  = "/usr/share/\(ProgramMetadata.NAME)/pkg"
    public static let MANIFESTS = "/usr/share/\(ProgramMetadata.NAME)/manifest"
    public static let LICENSES  = "/usr/share/\(ProgramMetadata.NAME)/licenses"

    public static let CACHE     = "/var/cache/\(ProgramMetadata.NAME)"
    public static let PKG_BINS  = "\(CACHE)/pkg"
    public static let REPOS     = "\(CACHE)/repo"

    public static let TMP       = "/tmp/\(ProgramMetadata.NAME)"
    public static let BUILDS    = "\(TMP)/build"
    public static let INST_TGTS = "\(TMP)/install"
    public static let BUILT_PKG = "\(TMP)/pkg"

    public static let CONFIG    = "\(ETC)/\(ProgramMetadata.NAME).conf"
    public static let ETC       = "/etc/\(ProgramMetadata.NAME)"

    public static let ALL_DIRS = [PACKAGES, MANIFESTS, LICENSES, CACHE, PKG_BINS, REPOS, TMP, BUILDS, INST_TGTS, BUILT_PKG, ETC]
}
