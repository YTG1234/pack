import Foundation
import Pack
import ArgumentParser
import TOMLDecoder

struct Pack: ParsableCommand {
    static var configuration = CommandConfiguration(
      abstract: "A package manager",
      version: ProgramMetadata.VERSION,

      subcommands: [Remote.self, Local.self, File.self],
      defaultSubcommand: Remote.self
    )
}

struct Options: ParsableArguments {
    @Flag(name: .shortAndLong, help: "Print out a list of all packages")
    var list = false
}

struct InstallationOptions: ParsableArguments {
    @Flag(name: .shortAndLong, help: "In case the manifest file has both buildscript and binary set, install from binary")
    var binary = false

    @Flag(name: .shortAndLong, help: "In case the manifest file has both buildscript and binary set, compile from source")
    var source = false
}

extension Pack {
    struct Remote: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "Handle remote packages",

          subcommands: [Install.self, Sync.self],
          defaultSubcommand: Install.self
        )

        @OptionGroup var options: Options

        public mutating func run() throws {
            print("Remote: List: \(options.list)")
        }
    }

    struct Local: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "Handle local packages",

          subcommands: [Remove.self, List.self],
          defaultSubcommand: Remove.self
        )

        @OptionGroup var options: Options

        public mutating func run() throws {
            print("Local: List: \(options.list)")
        }
    }

    struct File: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "Handle files in the filesystem",

          subcommands: [Install.self, Query.self]
        )
    }
}

extension Pack.Remote {
    struct Install: ParsableCommand {
        static var configuration = CommandConfiguration(abstract: "Install packages")

        @Argument(help: "Packages to install")
        var packages: [String]

        @OptionGroup var installOpts: InstallationOptions

        public mutating func run() throws {
            ifDebug { print("Packages to install: \(packages)") }

            let contents = try FileManager.default.contentsOfDirectory(atPath: PathData.REPOS)
            if contents.count < config.repositories.count {
                error("Not all repositories are synced, run 'pack remote sync' to sync them")
            }

            try packages.forEach(tryInstallPackage)
        }

        private func figureOutInstallStrat() -> InstallationStrategy {
            if (installOpts.binary && installOpts.source) || (!installOpts.binary && !installOpts.source) {
                return config.installationStrategy
            }

            return installOpts.binary ? .binary : .buildscript
        }

        private func tryInstallPackage(_ name: String) throws {
            for repo in try repositories() {
                for pkgUrl in try repo.listPackages() where pkgUrl.0.metadata.name == name {
                    try installPackage(from: pkgUrl, using: figureOutInstallStrat())
                }
            }
        }
    }

    struct Sync: ParsableCommand {
        static var configuration = CommandConfiguration(abstract: "Synchronise the local copies of the repositories with remote")

        public mutating func run() throws {
            ifDebug { print("Syncing repositories \(config.repositories)") }

            try repositories().forEach{ try $0.download() }
        }
    }
}

extension Pack.Local {
    struct Remove: ParsableCommand {
        static var configuration = CommandConfiguration(abstract: "Uninstall packages")

        @Argument(help: "Packages to uninstall")
        var packages: [String]

        public mutating func run() throws {
            print("Packages to uninstall: \(packages)")
        }
    }

    struct List: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "List all files installed by a package"
        )

        @Argument(help: "The package to list the files for")
        var package: String

        public mutating func run() throws {

        }
    }
}

extension Pack.File {
    struct Install: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "Install a package from its manifest"
        )

        @Option(name: .shortAndLong, help: "The manifest file to install from (must have the binary or buidlscript option set)")
        var manifest: String

        @OptionGroup var installOpts: InstallationOptions

        public mutating func run() throws {
            guard !(installOpts.binary && installOpts.source) else {
                error("Binary and source should not be specified at the same time!")
                return
            }
            let mUrl = URL(fileURLWithPath: manifest)

            let pkg = try TOMLDecoder().decode(Package.self, from: String(contentsOf: mUrl))

            guard !(installOpts.binary && pkg.retrieval.binary == nil) else {
                error("Binary option was specified but package metadata does not have a binary URL!")
                return
            }

            guard !(installOpts.source && pkg.retrieval.buildscript == nil) else {
                error("Source option was specified but package metadata does not have a buildscript URL!")
                return
            }

            try installPackage(
              from: (pkg, mUrl),
              using: installOpts.binary || installOpts.source ? (
                installOpts.binary ? .binary : .buildscript
              ) : config.installationStrategy
            )
        }
    }

    struct Query: ParsableCommand {
        static var configuration = CommandConfiguration(
          abstract: "Locate the package that owns a file"
        )

        @Argument(help: "The file to query")
        var file: String

        public mutating func run() throws {

        }
    }
}
