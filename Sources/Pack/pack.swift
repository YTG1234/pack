import Foundation
import Pack
import TOMLDecoder

fileprivate let DEFAULT_CONFIG = """
  {
    "envVars": {
        "COMMONFLAGS": "-march=native -O2 -pipe",
        "CFLAGS": "{COMMONFLAGS}",
        "CXXFLAGS": "{COMMONFLAGS}",
        "RUSTFLAGS": ""
    },
    "repositories": [],
    "installationStrategy": "binary"
  }
  """


public let config = (try? JSONDecoder().decode(Configuration.self, from: String(contentsOf: URL(fileURLWithPath: PathData.CONFIG)).data(using: .utf8)!)) ?? {
    try! createDirectories()
    FileManager.default.createFile(atPath: PathData.CONFIG, contents: Data(DEFAULT_CONFIG.utf8))
    error("The configuration file has been generated. Please run Pack again.\n\n\(DEFAULT_CONFIG)")
    return .empty
}()

fileprivate var _repositories: [RepoProvider]?

public func repositories() throws -> [RepoProvider] {
    if let r = _repositories {
        return r
    } else {
        throw PackError.repositoriesNotYetInitialized
    }
}

fileprivate func createDirectories() throws {
    for dir in PathData.ALL_DIRS where !FileManager.default.fileExists(atPath: dir) {
        ifDebug { print("Creating directory \(dir)") }

        try FileManager.default.createDirectory(
          atPath: dir, withIntermediateDirectories: true,
          attributes: nil
        )
    }
}

@main
struct PackProgram {
    static func main() throws {
        addProviders()

        _repositories = try config.repositories.map { object in
            for pType in getRegisteredProviders() {
                if let repo = try pType.init(from: object, in: PathData.REPOS) {
                    return repo
                }
            }

            throw PackError.noProviderFoundForRepo(object)
        }

        try createDirectories()

        Pack.main()
    }
}
