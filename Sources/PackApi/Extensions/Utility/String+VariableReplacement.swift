public extension String {
    func replaceVar(_ vars: [String:String]) -> String {
        return replaceVars(in: self, using: vars)
    }
}
