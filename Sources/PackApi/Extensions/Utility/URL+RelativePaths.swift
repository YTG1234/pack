import Foundation

public extension URL {
    /// Calculates an absolute path for ``dest``, starting at ``self``.
    ///
    /// - Parameters:
    ///    - dest: The destination relative path.
    ///    - isDirectory: Whether ``dest`` is a directory
    ///
    /// - Returns: The calculated absolute path.
    func calculateRPath(to dest: String, isDirectory: Bool = false) -> URL {
        if dest.hasPrefix("/") { return URL(fileURLWithPath: dest).standardized }

        return dest
          .split(separator: "/")
          .map(String.init)
          .reduce(self.absoluteString.hasSuffix("/") ? self.standardized : self.standardized.deletingPathExtension().deletingLastPathComponent()) { (acc, curr) in
            acc.appendingPathComponent(curr, isDirectory: isDirectory)
          }
          .standardized
    }
}
