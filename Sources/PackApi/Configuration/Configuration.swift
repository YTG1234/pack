import Foundation

/// Configuration for the Pack package manager.
public struct Configuration {
    /// Environment variables that will be set during the build.
    public let envVars: [String:String]

    /// The repositories to pull package metadata from.
    public let repositories: [JsonObject]

    /// Whether to compile packages from source by default.
    public let installationStrategy: InstallationStrategy

    public static let empty = Configuration(envVars: [:], repositories: [], installationStrategy: .binary)
}

extension Configuration: Codable {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.init(
            envVars: (try? values.decode([String:String].self, forKey: .envVars)) ?? [:],
            repositories: (try? values.decode([JsonObject].self, forKey: .repositories)) ?? [],
            installationStrategy: try values.decode(InstallationStrategy.self, forKey: .installationStrategy)
        )
    }

    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)

        if !envVars.isEmpty { try values.encode(envVars, forKey: .envVars )}
        if !repositories.isEmpty { try values.encode(repositories, forKey: .repositories) }

        try values.encode(installationStrategy, forKey: .installationStrategy)
    }

    enum CodingKeys: String, CodingKey {
        case envVars, repositories, installationStrategy
    }
}
