/// A container for all of the package metadata types.
///
/// They are devided into separate types because TOML.
public struct Package: Codable {
    /// See documentation for ``PackageRetrievalInfo``.
    public let retrieval: PackageRetrievalInfo

    /// See documentation for ``PackageMetadata``.
    public let metadata: PackageMetadata

    /// See documentation for ``PackageBuildInfo``.
    public let build: PackageBuildInfo

    /// See documentation for ``PackageMetadata2``.
    public let metadata2: PackageMetadata2

    /// Initializes a ``Package`` instance from all the properties of the included properties,
    /// rather than from the metadata types themselves.
    ///
    /// See individual metadata types' documentation for more info.
    public init(
      binary: String? = nil, buildscript: String? = nil,
      name: String, version: String, dependencies: [String] = [], provides: [String] = [], succeedes: [String] = [], conflicts: [String] = [],
      sources: [String] = [], sums: [String] = [],
      friendlyName: String? = nil, license: License = .custom(), description: String, longDescription: String? = nil) {
        self.retrieval = PackageRetrievalInfo(
          binary: binary,
          buildscript: buildscript
        )

        self.metadata = PackageMetadata(
          name: name,
          version: version,
          dependencies: dependencies,
          provides: provides,
          succeedes: succeedes,
          conflicts: conflicts
        )

        self.build = PackageBuildInfo(
          sources: sources,
          sums: sums
        )

        self.metadata2 = PackageMetadata2(
          friendlyName: friendlyName,
          license: license,
          description: description,
          longDescription: longDescription
        )
    }
}

extension Package {
    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)

        if !retrieval.isEmpty { try values.encode(retrieval, forKey: .retrieval) }
        try values.encode(metadata, forKey: .metadata)
        if !build.isEmpty { try values.encode(build, forKey: .build) }
        try values.encode(metadata2, forKey: .metadata2)
    }
}
