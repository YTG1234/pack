/// A certain way a package can be installed in.
public enum InstallationStrategy: String, Codable {
    /// Building from source.
    case buildscript

    /// Installing a binary.
    case binary
}
