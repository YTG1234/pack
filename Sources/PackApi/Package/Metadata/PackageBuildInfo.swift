/// Information that is given to the package buildscript.
public struct PackageBuildInfo: Codable, IsEmpty {
    /// Typically used to store the locations of various files
    /// needed during the compilation process.
    public let sources: [String]

    /// Typically used to contain the needed checksums of the
    /// files inside ``sources``.
    public let sums: [String]

    public var isEmpty: Bool {
        sources.isEmpty && sums.isEmpty
    }

    public init(sources: [String], sums: [String]) {
        self.sources = sources
        self.sums = sums
    }
}

extension PackageBuildInfo {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.init(
          sources: (try? values.decode([String].self, forKey: .sources)) ?? [],
          sums: (try? values.decode([String].self, forKey: .sums)) ?? []
        )
    }

    public func encode(to encoder: Encoder) throws {
        guard !isEmpty else { return }

        var values = encoder.container(keyedBy: CodingKeys.self)

        if !sources.isEmpty {
            try values.encode(sources, forKey: .sources)
        }
        if !sums.isEmpty {
            try values.encode(sums, forKey: .sums)
        }
    }

    enum CodingKeys: String, CodingKey {
        case sources, sums
    }
}
