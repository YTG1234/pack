/// More user-facing metadata, of which only the license the
/// package manager should care about.
public struct PackageMetadata2: Codable {
    /// A user-friendly name, typically in Title Case.
    public let friendlyName: String?

    /// A valid software license, represented as a
    /// ``License`` case.
    public let license: License

    /// A short and concise description of this package.
    public let description: String

    /// A longer, more detailed description of this package.
    public let longDescription: String?

    public init(friendlyName: String?, license: License, description: String, longDescription: String?) {
        self.friendlyName = friendlyName
        self.license = license
        self.description = description
        self.longDescription = longDescription
    }
}

extension PackageMetadata2 {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.init(
          friendlyName:     try? values.decode(String.self, forKey: .friendlyName),
          license:          License(try values.decode(String.self, forKey: .license))!,
          description:      try values.decode(String.self, forKey: .description),
          longDescription:  try? values.decode(String.self, forKey: .longDescription)
        )
    }

    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)

        if let friendlyName = friendlyName {
            try values.encode(friendlyName, forKey: .friendlyName)
        }

        try values.encode(String(license), forKey: .license)
        try values.encode(description, forKey: .description)

        if let longDescription = longDescription {
            try values.encode(longDescription, forKey: .longDescription)
        }
    }

    enum CodingKeys: String, CodingKey {
        case friendlyName, license,
             description, longDescription
    }
}
