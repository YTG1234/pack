/// Important metadata used by the package manager.
public struct PackageMetadata: Codable {
    /// A unique identifier for this package.
    public let name: String

    /// A SemVer-compliant version for this package.
    public let version: String

    /// A list of other package names that this package depends
    /// on.
    ///
    /// A SemVer version range may optionally be added after any
    /// package name, indicating a version requirement.
    public let dependencies: [String]

    /// A list of other pacakges that should be resolved to this
    /// package as dependencies.
    ///
    /// An equal sign (`=`) and a version number may be added after
    /// any package name, specifying the provided version.
    public let provides: [String]

    /// A list of package names that the package manager should
    /// immediately prompt to replace with this package.
    ///
    /// This should *only* be used in cases where a package changes its ID
    /// or when this package is a maintained fork of another package which
    /// has been abandoned.
    public let succeedes: [String]

    /// A list of package names that this package cannot be
    /// installed together with. When trying to do that, the package
    /// manager will simply fail.
    public let conflicts: [String]

    public init(name: String, version: String, dependencies: [String], provides: [String], succeedes: [String], conflicts: [String]) {
        self.name = name
        self.version = version
        self.dependencies = dependencies
        self.provides = provides
        self.succeedes = succeedes
        self.conflicts = conflicts
    }
}

extension PackageMetadata {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.init(
          name: try values.decode(String.self, forKey: .name),
          version: try values.decode(String.self, forKey: .version),
          dependencies: (try? values.decode([String].self, forKey: .dependencies)) ?? [],
          provides: (try? values.decode([String].self, forKey: .provides)) ?? [],
          succeedes: (try? values.decode([String].self, forKey: .succeedes)) ?? [],
          conflicts: (try? values.decode([String].self, forKey: .conflicts)) ?? []
        )
    }

    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)

        try values.encode(name, forKey: .name)
        try values.encode(version, forKey: .version)

        if !dependencies.isEmpty {
            try values.encode(dependencies, forKey: .dependencies)
        }
        if !provides.isEmpty {
            try values.encode(provides, forKey: .provides)
        }
        if !succeedes.isEmpty {
            try values.encode(succeedes, forKey: .succeedes)
        }
        if !conflicts.isEmpty {
            try values.encode(conflicts, forKey: .conflicts)
        }
    }

    enum CodingKeys: String, CodingKey {
        case name, version, dependencies, provides, succeedes, conflicts
    }
}
