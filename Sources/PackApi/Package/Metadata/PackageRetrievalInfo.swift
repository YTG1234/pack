/// Information related to downloading this package.
public struct PackageRetrievalInfo: Codable, IsEmpty {

    /// A URL leading to a package tarball, or a relative path.
    ///
    /// This should probably not be inside the repository, at
    /// least not on the default branch, as Pack will clone it.
    ///
    /// Instead, it can be a separate URL or another branch of
    /// this repo.
    public let binary: String?

    /// A URL leading to a package buildscript, or a relative path.
    ///
    /// Buildscripts are simply text files - so they don't tend
    /// to take up much space, so there's no harm in including
    /// the buildscripts inside the repository, making it a relative
    /// path.
    public let buildscript: String?

    public var isEmpty: Bool {
        binary == nil && buildscript == nil
    }

    public init(binary: String?, buildscript: String?) {
        self.binary = binary
        self.buildscript = buildscript
    }
}

extension PackageRetrievalInfo {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.init(
          binary: try? values.decode(String.self, forKey: .binary),
          buildscript: try? values.decode(String.self, forKey: .buildscript)
        )
    }

    public func encode(to encoder: Encoder) throws {
        guard !isEmpty else { return }

        var values = encoder.container(keyedBy: CodingKeys.self)

        if let binary = binary {
            try values.encode(binary, forKey: .binary)
        }
        if let buildscript = buildscript {
            try values.encode(buildscript, forKey: .buildscript)
        }
    }

    enum CodingKeys: String, CodingKey {
        case binary, buildscript
    }
}
