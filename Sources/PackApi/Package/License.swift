import Foundation

/// A valid software license representation.
///
/// For GNU software licenses (like `GPL` and `LGPL`), an `O` after
/// the name means that it the package is licensed under ONLY this
/// version, and nothing after the name means that the package is
/// licensed under this version or later.
///
/// The following licenses require a license file be installed to
/// `/usr/share/pack/licenses/$packageName/`:
/// * ``License/BSD``
/// * ``License/ISC``
/// * ``License/MIT``
/// * ``License/zlib``
/// * ``License/Python``
/// * ``License/OFL``
///
/// If using the ``License/Custom`` license type, a license name is optional and
/// a license file MUST be installed to `/usr/share/pack/licenses/$packageName/`.
public enum License {
	// GNU Licenses
	case
	  GPL2O,    GPL2,   GPL3O,  GPL3,
	  LGPL2O,   LGPL2,  LGPL3O, LGPL3,
	  AGPL2O,   AGPL2,  AGPL3O, AGPL3

	// A
	case Apache2
	case Artistic2

	// B
	case Boost
	case BSD

	// C
	case CCPL
	case CDDL
	case CPL

	// E
	case EPL

	// F
	case FDL1_2
	case FDL1_3

	// I
	case ISC

	// L
	case LPPL

	// M
	case MIT
	case MPL
	case MPL2

	// O
	case OFL

	// P
	case PerlArtistic
	case PHP
	case PSF
	case Python

	// R
	case RUBY

	// U
	case Unlicense

	// W
	case W3C

	// Z
	case ZPL
	case zlib


	case custom(name: String? = nil)
}

extension License: LosslessStringConvertible {
	public init?(_ description: String) {
		if description.hasPrefix("custom:") {
			self = .custom(name: description.substring(from: 6))
		}

		switch description {
		case let x where
			x.hasPrefix("custom:"):
								self = .custom(name: description.substring(from: 6))

		// When you don't have a convenience method for this
		case "GPL2O":           self = .GPL2O
		case "GPL2":            self = .GPL2
		case "GPL3O":           self = .GPL3O
		case "GPL3":            self = .GPL3
		case "LGPL2O":          self = .LGPL2O
		case "LGPL2":           self = .LGPL2
		case "LGPL3O":          self = .LGPL3O
		case "LGPL3":           self = .LGPL3
		case "AGPL2O":          self = .AGPL2O
		case "AGPL2":           self = .AGPL2
		case "AGPL3O":          self = .AGPL3O
		case "AGPL3":           self = .AGPL3

		case "Apache2":         self = .Apache2
		case "Artistic2":       self = .Artistic2

		case "Boost":           self = .Boost

		case "CCPL":            self = .CCPL
		case "CDDL":            self = .CDDL
		case "CPL":             self = .CPL

		case "EPL":             self = .EPL

		case "FDL1_2",
			 "FDL1.2":          self = .FDL1_2
		case "FDL1_3",
			 "FDL1.3":          self = .FDL1_3

		case "LPPL":            self = .LPPL

		case "MPL":             self = .MPL
		case "MPL2":            self = .MPL2

		case "PerlArtistic":    self = .PerlArtistic
		case "PHP":             self = .PHP
		case "PSF":             self = .PSF

		case "RUBY":            self = .RUBY

		case "Unlicense":       self = .Unlicense

		case "W3C":             self = .W3C

		case "ZPL":             self = .ZPL

		case "MIT":             self = .MIT
		case "BSD":             self = .BSD
		case "ISC":             self = .ISC
		case "zlib":            self = .zlib
		case "Python":          self = .Python
		case "OFL":             self = .OFL

		default:                return nil
		}
	}

	public var description: String {
		switch self {
		case .custom(let name): if let name = name {
									return "custom:\(name)"
								} else {
									return "custom"
								}
		default:
			return "\(self)"
		}
	}
}

extension License: Codable {
	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(self.description)
	}

	public init(from decoder: Decoder) throws {
		try self.init(decoder.singleValueContainer().decode(String.self))!
	}
}
