/// A single-property protocol informing about
/// whether the conforming type's properties are entirely
/// empty.
public protocol IsEmpty {
    var isEmpty: Bool { get }
}
