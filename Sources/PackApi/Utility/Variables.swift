import Foundation
import TOMLDecoder

public let manifestDecoder = run { () -> TOMLDecoder in
    let dec = TOMLDecoder()
    dec.keyDecodingStrategy = .convertFromSnakeCase

    return dec
}

public let manifestDecoderJson = run { () -> JSONDecoder in
    let dec = JSONDecoder()
    dec.keyDecodingStrategy = .convertFromSnakeCase

    return dec
}

public let manifestEncoder = run { () -> JSONEncoder in
    let enc = JSONEncoder()
    enc.keyEncodingStrategy = .convertToSnakeCase

    return enc
}