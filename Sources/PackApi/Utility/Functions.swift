import Foundation
import FoundationNetworking

fileprivate var stderr = FileHandle.standardError

/// Run the specified closure and return its value.
///
/// - Parameter closure: The closure to run.
public func run<Return>(_ closure: () -> Return) -> Return {
    closure()
}

/// Run a closure if the program is running
/// in the debug configuration.
///
/// - Parameter f: The closure to run.
public func ifDebug(f: () -> ()) {
    #if DEBUG
    f()
    #endif
}

/// When in the debug configuration, run a closure and return its result.
/// Otherwise, run another closure and return its result.
///
/// - Parameters:
///   - f: The closure to run in debug mode.
///   - ifElse: The closure to run if not in debug mode.
///
/// - Returns: The result of the closure that was run.
public func ifDebug<T>(f: () -> T, else ifElse: () -> T) -> T {
    #if DEBUG
    run(f)
    #else
    run(ifElse)
    #endif
}

/// Write provided arguments to stderr.
///
/// Parameters are the exact same as `print(:separator:terminator:)`
public func stdErr(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    for item in items {
        print(item, terminator: separator, to: &stderr)
    }
    print(terminator: terminator, to: &stderr)
}

/// Exit the program with error status.
public func errorOut() {
    exit(1)
}

/// Write provided arguments to stderr and exit with error status.
///
/// Parameters are the exact same as `print(:separator:terminator:)`
public func error(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    for item in items {
        stdErr(item, terminator: " ")
    }
    stdErr(terminator: terminator)

    errorOut()
}

public func sendRequest(target: URL) throws -> Data {
  let grp = DispatchGroup()
  var result: Data = Data()
  var errorW: Error? = nil

  grp.enter()
  URLSession.shared.dataTask(with: target) {(data, _, error) in
    guard let data = data else {
      errorW = error!
      grp.leave()
      return
    }

    result = data
    grp.leave()
  }.resume()

  grp.wait()

  if let err = errorW {
      throw err
  }

  return result
}

public func replaceVars(in target: String, using vars: [String:String]) -> String {
    var result = target
    for (key, value) in vars {
        result = result.replacingOccurrences(of: "{\(key)}", with: value)
    }
    return result
}
