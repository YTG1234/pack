public enum PackError: Error {
    case noProviderFoundForRepo(JsonObject)
    case repositoriesNotYetInitialized
    case repositoryManifestNotFound(String)
    case repositoryNotYetInitialized(String)
}
