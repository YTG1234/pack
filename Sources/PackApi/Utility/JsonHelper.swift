/// Represents a value that is codable in basic JavaScript Object
/// Notation, that is:
///
/// - Floating-point numbers
/// - Integers
/// - Booleans
/// - Strings
/// - Arrays
/// - Objects
/// - Null
public enum JsonRepresentable: Codable {
    case
      floating(Double),
      integer(Int),
      boolean(Bool),
      string(String),
      array([JsonRepresentable]),
      object([String:JsonRepresentable]),
      null

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        if let i = try? container.decode(Int.self) {
            self = .integer(i)
        } else if let f = try? container.decode(Double.self) {
            self = .floating(f)
        } else if let b = try? container.decode(Bool.self) {
            self = .boolean(b)
        } else if let s = try? container.decode(String.self) {
            self = .string(s)
        } else if let a = try? container.decode([JsonRepresentable].self) {
            self = .array(a)
        } else if let o = try? container.decode([String:JsonRepresentable].self) {
            self = .object(o)
        } else if container.decodeNil() {
            self = .null
        } else {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Failed to decode a JsonRepresentable")
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .floating(let f):  try container.encode(f)
        case .integer(let i):   try container.encode(i)
        case .boolean(let b):   try container.encode(b)
        case .string(let s):    try container.encode(s)
        case .array(let a):     try container.encode(a)
        case .object(let o):    try container.encode(o)
        case .null:             try container.encodeNil()
        }
    }
}

public typealias JsonArray = [JsonRepresentable]
public typealias JsonObject = [String:JsonRepresentable]
