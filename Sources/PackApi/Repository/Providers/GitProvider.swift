import Foundation
import TOMLDecoder

public class GitProvider: RepoProvider {
    public let url: URL
    public let name: String
    private let repositoryFolder: String
    private var metadata: GitRepoMetadata? = nil

    private var repoPath: String {
        repositoryFolder + "/" + name
    }

    public required init?(from json: JsonObject, in repositoryFolder: String) throws {
        self.repositoryFolder = repositoryFolder
        guard let url = json["url"] else { return nil }

        switch url {
        case .string(let url):
            guard let url = URL(string: url) else { return nil }
            self.url = url

            guard let name = json["name"] else {
                if url.pathExtension == "git" {
                    self.name = url.lastPathComponent.substring(to: url.lastPathComponent.count - 4)
                } else {
                    self.name = url.lastPathComponent
                }
                return
            }

            switch name {
            case .string(let name): self.name = name
            default: return nil
            }
        default: return nil
        }

        if let manifestData = FileManager.default.contents(atPath: "\(repoPath)/Manifest.toml") {
            self.metadata = try TOMLDecoder().decode(GitRepoMetadata.self, from: manifestData)
        }
    }

    public func download() throws {
        let proc = Process()
        proc.executableURL = URL(fileURLWithPath: "/usr/lib/git-core/git-clone")
        proc.currentDirectoryURL = URL(fileURLWithPath: repositoryFolder)
        proc.arguments = [url.absoluteString, repoPath]

        try proc.run()
        proc.waitUntilExit()

        guard let manifestData = FileManager.default.contents(atPath: "\(repoPath)/Manifest.toml") else {
            throw PackError.repositoryManifestNotFound(repoPath)
        }
        self.metadata = try TOMLDecoder().decode(GitRepoMetadata.self, from: manifestData)
    }

    private func listPackages(for folder: String) throws -> [(Package, URL)] {
        try FileManager.default.contentsOfDirectory(atPath: folder).reduce([]) { (acc, item) in
            guard let data = FileManager.default.contents(atPath: folder + "/" + item) else { return acc }
            return acc + [
              (
                try TOMLDecoder().decode(Package.self, from: data),
                URL.init(fileURLWithPath: folder + "/" + item)
              )
            ]
        }
    }

    public func listPackages() throws -> [(Package, URL)] {
        guard let mD = self.metadata else { throw PackError.repositoryNotYetInitialized(name) }
        return try listPackages(for: repoPath + "/" + mD.packages)
    }
}
