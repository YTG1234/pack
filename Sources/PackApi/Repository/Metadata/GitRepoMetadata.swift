public struct GitRepoMetadata: Codable {
    /// Relative to the root of the repository, the folder where package manifests are stored.
    public let packages: String
}
