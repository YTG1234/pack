fileprivate var registeredProviders: [RepoProvider.Type] = []

public func add(provider: RepoProvider.Type) {
    registeredProviders.append(provider)
}

public func getRegisteredProviders() -> [RepoProvider.Type] { registeredProviders }
