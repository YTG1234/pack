import Foundation

/// A Pack repository
public protocol RepoProvider {
    var name: String { get }
    /// Intialize the object from a dictionary, deserialized from Json.
    ///
    /// - Parameters:
    ///   - json: The object deserialized from Json.
    ///   - repositoryFolder: The folder that the repository should be stored in.
    ///
    /// - returns: `nil` if it can't be done
    init?(from json: JsonObject, in repositoryFolder: String) throws

    /// Download the contents of the repository to a folder.
    ///
    /// The repository should be downloaded to `(repositoryFolder)/(name)`.
    func download() throws

    /// List all of the installable packages in the repository.
    ///
    /// - Returns: The packages, represented by an array of tuples, the first item of which is the package metadata and the second is the path of the file where it is defined, to calculate relative paths from.
    func listPackages() throws -> [(Package, URL)]
}

extension RepoProvider {
    public func download() throws {}
}
