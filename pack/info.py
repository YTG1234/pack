from typing import Union

name: str
version: str
dependencies: list[str]
provides: list[str]
succeedes: list[str]
conflicts: list[str]

sources: list[str]
sums: list[str]

friendly_name: Union[str, None]
license_str: str
description: str
long_description: Union[str, None]

install_target: str
license_path: str

tarball_target: str
build_dir: str

buildscript_json: str
