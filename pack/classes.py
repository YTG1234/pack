import hashlib
from enum import Enum
from typing import Callable

class PackAction:
    def __init__(self, f: Callable = lambda: None, *args: object):
        self.f: Callable = f
        self.args: list[object] = [*args]

    def exec(self): self.f(*self.args)

class Actions:
    def __init__(self, *actions: PackAction):
        self.actions: list[PackAction] = [*actions]

    def exec(self):
        for action in self.actions:
            action.exec()

class HashingMethod(Enum):
    sha1 = hashlib.sha1
    sha256 = hashlib.sha256
    sha512 = hashlib.sha512

    md5 = hashlib.md5
