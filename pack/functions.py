import os
import tarfile
from pack.classes import Actions, HashingMethod, PackAction
from pack.info import *
from typing import Callable, Union

PackFunc = Callable[[], Actions]

setup_func: Union[PackFunc, None] = None
build_func: Union[PackFunc, None] = None
install_func: Union[PackFunc, None] = None

# Build Process
def download_file(url: str, save: Union[str, None]) -> PackAction:
    def doWork():
        import requests

        with requests.get(url) as file:
            if save == None: location = os.path.basename(url)
            else: location = save

            location = os.path.abspath(location)

            with open(location, "wb") as f:
                f.write(file.content)

    return PackAction(doWork)

def verify_file(name: str, sum: str, method: HashingMethod) -> PackAction:
    def verify():
        from sys import exit

        with open(name, "rb") as file:
            hash: str = method.value(file.read()).hexdigest()
            if hash != sum:
                print(f"File failed to verify! Expected '{sum}', got '{hash}' at {name}")
                exit(1)

    return PackAction(verify)

def extract_tarball(name: str, to: str) -> PackAction:
    def extract():
        with tarfile.open(os.path.abspath(name)) as file:
            file.extractall(f"./{to}")

    return PackAction(extract)

def cd(destination: str) -> PackAction:
    return PackAction(os.chdir, destination)

def call(cmd: str, *arguments: str) -> PackAction:
    def execCmd():
        import subprocess
        print(f"Called with {cmd} and {arguments}")
        subprocess.run([cmd, *arguments])

    return PackAction(execCmd)

def make(target: str = "", options: dict[str, str] = {}, *arguments: str) -> PackAction:
    optionsStr: list[str] = []
    for (key, val) in options.items():
        optionsStr.append(f'{key}={val}') # Mappings? Anyone?

    args: list[str] = []
    args.append("make")
    if len(target) > 0: args.append(target)
    if len(optionsStr) > 0: args.append(" ".join(optionsStr))
    if len(arguments) > 0: args.append(*arguments)

    return call(*args)

def copy(*, source: str, mode: int, dest: Union[str, None], destDir: Union[str, None]) -> PackAction:
    if dest != None:
        return call("install", "-D", f"-m{mode}", dest)
    elif destDir != None:
        return call("install", "-D", "-t", destDir, f"-m{mode}", source)
    else:
        print("Both dest and destDir were None!")
        exit(1)

def copy_files(*source: str, destDir: str) -> PackAction:
    return call("install", "-D", "-t", destDir, *source)

# Actually doing stuff
def _start_build() -> None:
    if setup_func == None: setupActions = Actions()
    else: setupActions = setup_func()

    if build_func == None: buildActions = Actions()
    else: buildActions = build_func()

    if install_func == None: installActions = Actions()
    else: installActions = install_func()

    os.chdir(build_dir)
    setupActions.exec()

    os.chdir(build_dir)
    buildActions.exec()

    os.chdir(build_dir)
    installActions.exec()

    with open("/tmp/version", "w") as file:
        file.write(version)

    with tarfile.open(tarball_target, "w:gz") as tar:
        tar.add("/tmp/version", arcname=".VERSION")
        for file in list(map(lambda x: install_target + "/" + x, os.listdir(install_target))):
            tar.add(file, arcname=os.path.basename(file))
